#!/usr/bin/env python
import os
import platform
import subprocess
def convert_texture(texture):
    """
    converting the textures to EXR mip mapping using OpenImageIO
    """
    if platform.system() == "Windows":
        file_In = texture
        file_Out = os.path.splitext(texture)[0]
        subprocess.Popen("maketx %s --resize -v -o %s" % (file_In, file_Out), shell=True)

    if platform.system() == "Linux":
        try:
            script = "%s/maketx.sh" % os.path.dirname(os.path.abspath(os.path.abspath(__file__)))
            file_In = texture
            file_Out = os.path.splitext(texture)[0]
            subprocess.Popen("sh %s %s %s.exr" % (script, file_In, file_Out), shell=True)
        except:
            pass

