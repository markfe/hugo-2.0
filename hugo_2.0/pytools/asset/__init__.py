import os
import re

def assetFind():
    path = os.environ['ASSETS']
    assets = [F for F in os.listdir(path) if re.match("[0-9]{1,4}_.+", F) and os.path.isdir(os.path.join(path, F))]
    assets.sort()
    return assets

def assetName(asset_id, asset_name, asset_type):
    """
    Users are users, we better check what they have typed
    """
    model_name = asset_name.replace("_","").replace(" ", "")
    name = "%s_%s_%s" % (asset_id, model_name,asset_type)
    return name

def asset_types():
    """ 
    only for softimage 
    """
    return ["cam", "cam", "geo", "geo", "rig", "rig", "assembly", "assembly"]

def assetNextId():
    assets = assetFind()
    next_id = "%04d" %(int(assets[len(assets) - 1])+1)
    return next_id
    
