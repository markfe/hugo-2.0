import os
import shutil
import platform
import re
import fileinput
import subprocess
from win32com.client import constants as C
from win32com.client import Dispatch
si = Dispatch('XSI.Application')

import pytools.util
reload(pytools.util)
import lib.util
from pytools.asset.softimage import asset_util

def format_asset_name(asset_id, asset_name, asset_type):
    """
    Users are users, we better check what they have typed
    """
    model_name = asset_name.replace("_","").replace(" ", "")
    name = "%s_%s_%s" % (asset_id, model_name,asset_type)
    return name

def export_Model(asset_id, asset_name, asset_type, update):
    """
    Export Asset GEO,MatLib, and change the texture paths before export 
    """ 

    asset_file = asset_name.replace("_","").replace(" ", "")  
    asset_folder_name = format_asset_name(asset_id, asset_name, asset_type)
    asset_model = si.Selection(0)
    asset_geo = si.Selection(0).FindChildren("",C.siPolyMeshType, C.siGeometryFamily, True)
    asset_path = lib.util.path_join(os.environ['ASSETS'], asset_folder_name)
    for folder in ['MatLib', "Textures"]:
        try:
            os.makedirs(lib.util.path_join(asset_path, folder), 0755)
        except:
            pass
    
    if asset_type == "geo":
        change_texure_path(asset_path, asset_geo)
        export_MatLib(asset_folder_name, asset_model, asset_path, asset_geo)
        for texture in asset_util.list_textures(asset_geo):
            pytools.util.convert_texture(texture)
    if not update:
        si.AddProp("Annotation", asset_model, "", "AssetInfo", "")
    si.SetValue("%s.AssetInfo.text" % asset_model, "id:%s\nname:%s\ntype:%s" % (asset_id, asset_name, asset_type), "")
    si.ExportModel(asset_model, "%s/%s_%s.emdl" % (asset_path, asset_file, asset_type), "", "")
    si.ExportObjectRenderArchive(asset_model, "%s/%s_%s" % (asset_path, asset_file, asset_type), 100, 100, 1, "", "")
    asset_version(asset_path, asset_file, asset_type)
    si.SaveScene()

def asset_version(asset_path, asset_file, asset_type):
    """
    make safe copy
    """
    try:    
        os.makedirs(lib.util.path_join(asset_path, "versions"))
    except:
        print "not creating: %s" % lib.util.path_join(asset_path, "versions")
        pass
    v = [version for version in os.listdir(lib.util.path_join(asset_path, "versions")) if version.startswith("v00")]
    if len(v)==0: versionNumber=1
    else: versionNumber = int(v[len(v)-1].lstrip("v"))+1
    versionPath = lib.util.path_join(asset_path, "versions", "v%05d" % versionNumber)
    print versionPath
    try:    
        os.makedirs(versionPath)
    except:
        pass
    shutil.copy("%s/%s_%s.emdl" % (asset_path, asset_file, asset_type), "%s/%s_%s.emdl" % (versionPath, asset_file, asset_type))
    
def change_texure_path(asset_path, asset_geo):
    """
    Change texture path to be relative to the exported asset
    """
    asset_texture_path = "%s/Textures" % asset_path
    for geo in asset_geo:
        for img in geo.Material.ImageClips:
            texture = os.path.basename(img.Source.Path.Value)
            print texture
            try:
                shutil.copy(img.Source.FileName.Value,"%s/%s" % (asset_texture_path, texture))
                pytools.util.convert_texture(lib.util.path_join(asset_texture_path,texture))
            except:
                pass
            si.SetValue(img.Source.FileName,"%s/%s.exr" % (asset_texture_path, os.path.splitext(texture)[0]))

def parse_MatLib(MatLib,asset_geo):
    """ 
    Parse through the MatLib and replace texture file paths to be relative
    """
    for img in asset_util.list_textures(asset_geo):
        for line in fileinput.FileInput(MatLib, inplace=1):
            if img in line:
                a = line.split('\t')
                line = line.replace(a[len(a)-1], "Textures/%s" % img)
            print line,


def export_MatLib(asset_name, asset_model, asset_path, asset_geo):
    """
    Export Asset MatLib
    """
    Shaders, MatLib = asset_util.create_MatLib( asset_model, asset_geo )
    si.moveToLibrary(Shaders, MatLib)
    asset_MatLib = "%s.xsi" % os.path.join(asset_path, "MatLib", asset_name)
    si.ExportMaterialLibrary(asset_MatLib, MatLib, "")
    parse_MatLib(asset_MatLib,asset_geo)

