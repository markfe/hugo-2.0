import os
import shutil
import platform
import re
import fileinput
import subprocess
from win32com.client import constants as C
from win32com.client import Dispatch
si = Dispatch('XSI.Application')


def create_MatLib(model, asset_geo):
    """ 
    Create MatLib and move assets shaders under MatLib 
    """
    try:
        si.SetCurrentMaterialLibrary("Sources.Materials.%s" % model)
    except:
        si.createLibrary(model.Name)
    MatLib = si.ActiveProject.ActiveScene.ActiveMaterialLibrary
    Shaders = ",".join([geo.Material.FullName for geo in asset_geo])
    return Shaders, MatLib


def list_textures(asset_geo):
    """ 
    List all images connected to shaders 
    """
    tex = set([img.Source.Path.Value for geo in asset_geo for img in geo.Material.ImageClips ])
    return tex






        
