# ex 000_000_testing_001a_sa.scn = seq, shot, description, version, user
import os
scene_name = Application.ActiveProject.ActiveScene.Name.lstrip("_")
seq, shot, description, version, user = scene_name.replace("_", " ").split(" ", 4)
render_folder = "%s_%s_%s/%s" % (seq, shot, description, version)

#global values
Application.SetValue("Passes.RenderOptions.FramePadding", 4, "")
Application.SetValue("Passes.RenderOptions.OutputDir", "[Project Path]/Render_Pictures/%s" % render_folder, "")
#Application.SetValue("Passes.mentalray.FGMapFile", "%s/AppData/Local/Temp/render.fgmap" % os.environ["USERPROFILE"], "")

#pass values
for renderpass in Application.ActiveProject.ActiveScene.Passes:
    for buffer in renderpass.FrameBuffers:
        Application.SetValue("%s.Format" % buffer, "exr", "")
        Application.SetValue("%s.Filename" % buffer, "%s_%s_%s.%s" % (seq, shot, renderpass.Name, buffer.Name), "")

