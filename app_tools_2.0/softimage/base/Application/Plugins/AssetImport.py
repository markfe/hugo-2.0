#!/usr/bin/env python 
import os
import sys
sys.path.append(os.environ["HUGO"])

from win32com.client import constants as C
from win32com.client import Dispatch
si = Dispatch('XSI.Application')
from lib import util
import pytools.asset
from pytools.asset.softimage import asset_util
from pytools.asset.softimage import asset_export


def XSILoadPlugin( in_reg ):
    in_reg.Author = "stefan"
    in_reg.Name = "AssetImportPlugin"
    in_reg.Major = 1
    in_reg.Minor = 0
    in_reg.RegisterProperty("AssetImport")
    in_reg.RegisterMenu(C.siMenu3DViewObjectContextID,"AssetImport_Menu",False,False)
    #RegistrationInsertionPoint - do not remove this line

    return True

def XSIUnloadPlugin( in_reg ):
    strPluginName = in_reg.Name
    si.LogMessage(str(strPluginName) + str(" has been unloaded."),C.siVerbose)
    return True

def AssetImport_Define( in_ctxt ):
    prop = in_ctxt.Source
    prop.AddParameter2('Assets', C.siString, '', None, None, None, C.siReadOnly, C.siClassifMetaData, 0, 'Assets')
    prop.AddParameter2('asset_type', C.siString, 'all', None, None, None, C.siReadOnly, C.siClassifMetaData, 0, 'asset_type')
    prop.AddParameter2("search", C.siString, "" ,None,None,None,None,C.siClassifMetaData, 0, "search")
    return True

def list_assets():
    b = []
    a = pytools.asset.find_assets()
    for item in a:
        b.append("%s_%s %s" % (item.split("_")[0],item.split("_")[1],item.split("_")[2].upper()))
        b.append("%s_%s_%s" % (item.split("_")[0],item.split("_")[1],item.split("_")[2]))
    return b       
   
def AssetImport_OnInit( ):
    ui = PPG.PPGLayout
    ui.Clear()
    types = pytools.asset.asset_types()
    types.append('all')
    types.append('all')
    types.sort()
    item = ui.AddEnumControl("asset_type", types, "Asset Type")
    ui.AddRow()
    item = ui.AddItem("search")
    item.SetAttribute( "NoLabel", True )
    ui.AddButton("findAsset", "Find Asset")
    ui.EndRow()
    #item.SetAttribute( C.siUICX, 300 )
    item.SetAttribute( "NoLabel", True )
    item = ui.AddEnumControl('Assets', list_assets(), 'Assets', C.siControlListBox)
    item.SetAttribute(C.siUIMultiSelectionListBox, True)
    item.SetAttribute( "NoLabel", True )
    item.SetAttribute( C.siUICY, 300 )
    ui.AddRow()
    item = ui.AddButton("importReference", "Create reference")
    item.SetAttribute( C.siUICX, 100 )
    item.SetAttribute( C.siUICY, 50 )
    item = ui.AddButton("import", "import")
    item.SetAttribute( C.siUICX, 100 )
    item.SetAttribute( C.siUICY, 50 )
    item = ui.AddButton("mia", "Standin (*.mia, *.ass)")
    item.SetAttribute( C.siUICX, 100 )
    item.SetAttribute( C.siUICY, 50 )
    ui.EndRow()

    PPG.Refresh()

def AssetImport_findAsset_OnClicked():
    ui = PPG.PPGLayout
    new_asset_list = []
    asset_list = set(pytools.asset.find_assets())

    for item in asset_list:
        if PPG.search.Value in item:
            print PPG.search.Value, item
            new_asset_list.append(item)
            new_asset_list.append(item)
    new_asset_list.sort()

    filter_type = ui.Item('Assets')
    filter_type.UIItems = new_asset_list
    PPG.Refresh()


def AssetImport_importReference_OnClicked():
    emdl = "%s_%s.emdl" % (PPG.Assets.Value.split("_")[1], PPG.Assets.Value.split("_")[2])
    si.SICreateRefModel("%s" % util.path_join(os.environ["ASSETS"], PPG.Assets.Value, emdl), "", "", "", "", "", "", "")

def AssetImport_import_OnClicked():
    emdl = "%s_%s.emdl" % (PPG.Assets.Value.split("_")[1], PPG.Assets.Value.split("_")[2])
    si.ImportModel("%s" % util.path_join(os.environ["ASSETS"], PPG.Assets.Value, emdl), "", "", "", "", "", "")

def AssetImport_mia_OnClicked():
    mia = "%s_%s.mia" % (PPG.Assets.Value.split("_")[1], PPG.Assets.Value.split("_")[2])
    mia_path = util.path_join(os.environ["ASSETS"], PPG.Assets.Value, mia)
    if not os.path.exists(mia_path):
        XSIUIToolkit.MsgBox('Sorry no (*.mia, *.ass) file exists')
    else:
        mia_standin = si.GetPrim("Stand-in", "%s_%s" %(PPG.Assets.Value.split("_")[1], PPG.Assets.Value.split("_")[2]), "", "")
        si.SetValue("%s.standin.FileName" % mia_standin, mia_path, "")

def AssetImport_asset_type_OnChanged( ):
    ui = PPG.PPGLayout
    new_asset_list = []
    asset_list = set(pytools.asset.find_assets())
    if not PPG.asset_type.Value == "all":
        for item in asset_list:
            if item.split("_")[2]== PPG.asset_type.Value:
                new_asset_list.append(item)
                new_asset_list.append(item)
        new_asset_list.sort()
    else:
        new_asset_list = list_assets()
    filter_type = ui.Item('Assets')
    filter_type.UIItems = new_asset_list
    PPG.Refresh()




    
def AssetImport_OnClosed( ):
    si.DeleteObj("AssetImport")

def OnAssetImportMenuClicked( in_ctxt ):
    si.AddProp("AssetImport")
    return True


