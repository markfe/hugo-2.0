from win32com.client import constants as C
from win32com.client import Dispatch
si = Dispatch('XSI.Application')

def XSILoadPlugin( in_reg ):
    in_reg.Author = "Stefan Andersson"
    in_reg.Name = "Sanders3d_Menu"
    in_reg.Email = "sanders3d@gmail.com"
    in_reg.Major = 0
    in_reg.Minor = 1
    in_reg.RegisterMenu(C.siMenuMainTopLevelID,"HUGO", False, False)
    in_reg.RegisterMenu(C.siMenuMainFileSceneID,"HUGO", False, False)
    in_reg.RegisterCommand("AssetExport_cmd", "AssetExport_cmd")
    in_reg.RegisterCommand("AssetImport_cmd", "AssetImport_cmd")
    in_reg.RegisterCommand("AssetRevert_cmd", "AssetRevert_cmd")
    in_reg.RegisterCommand("renamer", "renamer")
    return True


def HUGO_Init(in_ctxt):
    menu = in_ctxt.Source
    assetMenu = menu.AddSubMenu("Assets")
    assetMenu.AddCommandItem("Export Asset","AssetExport_cmd")
    assetMenu.AddCommandItem("Import Asset","AssetImport_cmd")
    assetMenu.AddCommandItem("Revert Back (change asset version)","AssetRevert_cmd")
    utilMenu = menu.AddSubMenu("Tools")
    utilMenu.AddCommandItem("Renamer","renamer")
    return True	
    
def HUGO_Menu_Init(in_ctxt):
    return True	
    
def AssetExport_cmd_Execute():
    if si.Selection(0).IsClassOf(C.siModelID):
        prop = si.SIAddProp("AssetExport_UI", si.ActiveSceneRoot, "", "", "")
        si.InspectObj("AssetExport_UI")
    else:
        XSIUIToolkit.MsgBox('Selected Object is not a model')
    return True

def AssetImport_cmd_Execute():
    prop = si.SIAddProp("AssetImport", si.ActiveSceneRoot, "", "", "")
    si.InspectObj("AssetImport")
    return True    

def AssetRevert_cmd_Execute():
    prop = si.SIAddProp("AssetRevert", si.ActiveSceneRoot, "", "", "")
    si.InspectObj("AssetRevert")
    return True    

def renamer_Execute():
    prop = si.SIAddProp("Rename_Objects", si.ActiveSceneRoot, "", "", "")
    si.InspectObj("Rename_Objects")
    return True