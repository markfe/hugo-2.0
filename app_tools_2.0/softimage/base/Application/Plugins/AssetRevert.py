#!/usr/bin/env python 
import os
import sys
import shutil
sys.path.append(os.environ["HUGO"])

from win32com.client import constants as C
from win32com.client import Dispatch
import lib.util
import pytools.asset
from pytools.asset.softimage import asset_util
from pytools.asset.softimage import asset_export


def XSILoadPlugin( in_reg ):
    in_reg.Author = "stefan"
    in_reg.Name = "AssetRevertPlugin"
    in_reg.Major = 1
    in_reg.Minor = 0
    in_reg.RegisterProperty("AssetRevert")
    in_reg.RegisterMenu(C.siMenu3DViewObjectContextID,"AssetRevert_Menu",False,False)
    #RegistrationInsertionPoint - do not remove this line

    return True

def XSIUnloadPlugin( in_reg ):
    strPluginName = in_reg.Name
    Application.LogMessage(str(strPluginName) + str(" has been unloaded."),C.siVerbose)
    return True

def AssetRevert_Define( in_ctxt ):
    prop = in_ctxt.Source
    prop.AddParameter2('Assets', C.siString, '', None, None, None, C.siReadOnly, C.siClassifMetaData, 0, 'Assets')
    prop.AddParameter2('asset_type', C.siString, 'all', None, None, None, C.siReadOnly, C.siClassifMetaData, 0, 'asset_type')
    prop.AddParameter2('Version', C.siString, '', None, None, None, C.siReadOnly, C.siClassifMetaData, 0, 'Version')
    return True

def list_assets():
    b = []
    a = pytools.asset.find_assets()
    for item in a:
        b.append("%s_%s %s" % (item.split("_")[0],item.split("_")[1],item.split("_")[2].upper()))
        b.append("%s_%s_%s" % (item.split("_")[0],item.split("_")[1],item.split("_")[2]))
    return b       
   
def AssetRevert_OnInit( ):
    ui = PPG.PPGLayout
    ui.Clear()
    types = pytools.asset.asset_types()
    types.append('all')
    types.append('all')
    types.sort()
    item = ui.AddEnumControl("asset_type", types, "Asset Type")
    #item.SetAttribute( C.siUICX, 300 )
    item.SetAttribute( "NoLabel", True )
    ui.AddRow()
    item = ui.AddEnumControl('Assets', list_assets(), 'Assets', C.siControlListBox)
    item.SetAttribute(C.siUIMultiSelectionListBox, True)
    item.SetAttribute( "NoLabel", True )
    item.SetAttribute( C.siUICY, 450 )

    item = ui.AddEnumControl('Version', [], 'Version', C.siControlListBox)
    item.SetAttribute(C.siUIMultiSelectionListBox, True)
    item.SetAttribute( "NoLabel", True )
    item.SetAttribute( C.siUICY, 450 )
    ui.EndRow()

    ui.AddRow()
    item = ui.AddButton("RevertAsset", "Revert Asset")
    item.SetAttribute( C.siUICX, 300 )
    item.SetAttribute( C.siUICY, 50 )
    ui.EndRow()

    PPG.Refresh()
    
def AssetRevert_asset_type_OnChanged( ):  
    ui = PPG.PPGLayout
    new_asset_list = []
    asset_list = set(pytools.asset.find_assets())
    if not PPG.asset_type.Value == "all":
        for item in asset_list:
            if item.split("_")[2]== PPG.asset_type.Value:
                new_asset_list.append(item)
                new_asset_list.append(item)
        new_asset_list.sort()
    else:
        new_asset_list = list_assets()
    filter_type = ui.Item('Assets')
    filter_type.UIItems = new_asset_list
    Version = ui.Item('Version')
    Version.UIItems = []
    PPG.Refresh()
    
def AssetRevert_Assets_OnChanged():
    versions = []
    v = os.listdir(lib.util.path_join(os.environ["ASSETS"],PPG.Assets.Value,"versions"))
    for ver in v:
        versions.append(ver)
        versions.append(ver)
    versions.sort()
    ui = PPG.PPGLayout
    Version = ui.Item('Version')
    Version.UIItems = versions
    PPG.Refresh()
    #print pytools.asset.find_assets()

def AssetRevert_RevertAsset_OnClicked():
    assetPath = lib.util.path_join(os.environ["ASSETS"],PPG.Assets.Value)
    versionPath = lib.util.path_join(assetPath, "versions",PPG.Version.Value)
    emdl = [e for e in os.listdir(versionPath) if e.endswith(".emdl")][0]
    shutil.copy(lib.util.path_join(versionPath, emdl), lib.util.path_join(assetPath, emdl))
    print lib.util.path_join(versionPath, emdl), lib.util.path_join(assetPath, emdl)



def AssetRevert_OnClosed( ):
    Application.DeleteObj("AssetRevert")

def OnAssetRevertMenuClicked( in_ctxt ):
    Application.AddProp("AssetRevert")
    return True


