import os
import win32com.client
from win32com.client import constants as C

def XSILoadPlugin( in_reg ):
	in_reg.Author = "Stefan Andersson"
	in_reg.Name = "Rename_ObjectsPlugin"
	in_reg.Email = "sanders3d@gmail.com"
	in_reg.URL = ""
	in_reg.Major = 1
	in_reg.Minor = 0
	in_reg.RegisterProperty("Rename_Objects")
	return True

def XSIUnloadPlugin( in_reg ):
	strPluginName = in_reg.Name
	Application.LogMessage("Sanders3d Renamer has been unloaded")
	return True

def Rename_Objects_Define( ctxt ):
	oCustomProperty = ctxt.Source
	oCustomProperty.AddParameter2("Search",C.siString,"",None,None,None,None,False,C.siPersistable)
	oCustomProperty.AddParameter2("Object_Type", C.siString, "All",None,None,None,None, C.siClassifMetaData, False, "Object Types")
	oCustomProperty.AddParameter2("Prefix",C.siString,"",None,None,None,None,False,C.siPersistable)	
	oCustomProperty.AddParameter2("Suffix",C.siString,"",None,None,None,None,False,C.siPersistable)
	oCustomProperty.AddParameter2("Rename",C.siString,"",None,None,None,None,False,C.siPersistable)
	oCustomProperty.AddParameter2("Start_Number",C.siString,"1",None,None,None,None,False,C.siPersistable)
	oCustomProperty.AddParameter2("Padding",C.siString,"0",None,None,None,None,False,C.siPersistable)
	return True

def Rename_Objects_DefineLayout( ctxt ):
	oLayout = ctxt.Source
	oLayout.Clear()
	oLayout.AddTab("Rename Utility")
	oLayout.AddGroup("Search and Select")
	oLayout.AddItem("Search")
	oLayout.AddEnumControl("Object_Type", ["All", "All", "Cameras", "Cameras", "Lights", "Lights", "Curves", "Curves", "Polymesh", "Polymesh", "Nurbmesh", "Nurbmesh", "Null", "Null"])
	oLayout.AddButton("Search_and_Select", "Search and select")
	oLayout.EndGroup()
	oLayout.AddGroup("Change Prefix")
	oLayout.AddItem("Prefix")
	oLayout.AddButton("Add_Prefix", "Add Prefix")
	oLayout.EndGroup()
	oLayout.AddGroup("Change Suffix")
	oLayout.AddItem("Suffix")
	oLayout.AddButton("Add_Suffix", "Add Suffix")
	oLayout.EndGroup()
	oLayout.AddGroup("Rename and Number")
	oLayout.AddItem("Rename")
	oLayout.AddItem("Start_Number")
	oLayout.AddItem("Padding")
	oLayout.AddButton("Rename_and_Number", "Rename And Number")
	oLayout.EndGroup()
	return True

def Rename_Objects_OnInit( ):
	Application.LogMessage("Sanders3d Renamer has been loaded")

def Rename_Objects_Search_OnChanged( ):
	oParam = PPG.Search
	paramVal = oParam.Value

def Rename_Objects_Object_Type( ):
	oParam = PPG.Object_Type
	paramVal = oParam.Value

def Rename_Objects_Search_and_Select_OnClicked( ):
	xsi = Application
	if PPG.Object_Type.Value == ("Lights"):	
		thelist = xsi.activesceneroot.findchildren( "*" + PPG.Search.Value + "*", '', C.siLightPrimitiveFamily, True )
		if thelist.count < 1:
			xsi.LogMessage("No Lights exists in scene")
		else:
			xsi.SelectObj(thelist)
	
	elif PPG.Object_Type.Value == ("Cameras"):	
		thelist = xsi.activesceneroot.findchildren( "*" + PPG.Search.Value + "*", '', C.siCameraFamily, True )
		if thelist.count < 1:
			xsi.LogMessage("No Cameras exists in scene")
		else:
			xsi.SelectObj(thelist)
		
	elif PPG.Object_Type.Value == ("Null"):	
		thelist = xsi.activesceneroot.findchildren( "*" + PPG.Search.Value + "*", '', C.siNullPrimitiveFamily, True )
		if thelist.count < 1:
			xsi.LogMessage("No Null objects exists in scene")
		else:
			xsi.SelectObj(thelist)
		
	elif PPG.Object_Type.Value == ("Curves"):	
		thelist = xsi.activesceneroot.findchildren( "*" + PPG.Search.Value + "*", '', C.siNurbsCurveListFamily, True )
		if thelist.count < 1:
			xsi.LogMessage("No Curves exists in scene")
		else:
			xsi.SelectObj(thelist)

	elif PPG.Object_Type.Value == ("Polymesh"):	
		thelist = xsi.activesceneroot.findchildren( "*" + PPG.Search.Value + "*", '', C.siMeshFamily, True )
		if thelist.count < 1:
			xsi.LogMessage("No Polymesh exists in scene")
		else:
			xsi.SelectObj(thelist)

	elif PPG.Object_Type.Value == ("Nurbmesh"):	
		thelist = xsi.activesceneroot.findchildren( "*" + PPG.Search.Value + "*", '', C.siNurbsSurfaceMeshFamily, True )
		if thelist.count < 1:
			xsi.LogMessage("No Nurb meshes Exists in scene")
		else:
			xsi.SelectObj(thelist)
	else:
		thelist = xsi.activesceneroot.findchildren( "*" + PPG.Search.Value + "*", '', C.si3DObjectFamily, True )
		xsi.SelectObj(thelist)

def Rename_Objects_Suffix_OnChanged( ):
	oParam = PPG.Suffix
	paramVal = oParam.Value

def Rename_Objects_Add_Prefix_OnClicked( ):
	xsi = Application
	countObj = xsi.Selection.Count
	PreName = PPG.Prefix.Value
	for i in range(countObj):
		newname = PreName + xsi.Selection(i).Name
		xsi.Selection(i).Name = newname	
	
def Rename_Objects_Add_Suffix_OnClicked( ):
	xsi = Application
	countObj = xsi.Selection.Count
	Lastname = PPG.Suffix.Value
	for i in range(countObj):
		newname = xsi.Selection(i).Name + Lastname
		xsi.Selection(i).Name = newname
	

#----------------------------------------------------------------------------------
# rename and change padding
#----------------------------------------------------------------------------------

def Rename_Objects_Rename_OnChanged( ):
	oParam = PPG.Rename
	paramVal = oParam.Value

def Rename_Objects_Start_Number_OnChanged( ):
	oParam = PPG.Start_Number
	paramVal = oParam.Value

def Rename_Objects_Padding_OnChanged( ):
	oParam = PPG.Padding
	paramVal = oParam.Value

def Rename_Objects_Rename_and_Number_OnClicked( ):
	xsi = Application
	countObj = xsi.Selection.Count
	oObjs = (Application.Selection)
	Padding = PPG.Padding.Value
	Rename = PPG.Rename.Value
	Start_Number = PPG.Start_Number.Value
	extPad = '%0' + Padding + 'i'
	for i in range(countObj):
		xsi.Selection(i).Name = Rename + extPad % (i + int(Start_Number))


def Rename_Objects_Menu_Init( ctxt ):
	oMenu = ctxt.Source
	oMenu.AddCallbackItem("Rename_Objects","OnRename_ObjectsMenuClicked")
	return True

def Rename_Objects_OnClosed():
	Application.DeleteObj("Rename_Objects")

